---
title: "SNA Visualization Practice"
author: "Jeongsoo, Park"
date: "Friday, June 27, 2014"
---

```{r echo=FALSE}
options(device = "png")
par(family="Korea1deb")
opts_chunk$set(fig.align='center', prompt=TRUE)
opts_chunk$set(dev="png", dev.args=list(type="cairo", family="Korea1"), dpi=96)
setwd('C:/Users/toracle/projects/r-learning')
```

```{r echo=FALSE, eval=FALSE}
library(knitr)
opt <- c("use_xhtml","smartypants","mathjax","highlight_code", "base64_images")
knit2html('01_sna_visualization_practice.Rmd', encoding='utf-8', options=opt)
```

# R 둘러보기, 시각화 맛보기

----

## 환경 설정하기

실습을 진행하기 전에, Project부터 설정하겠습니다. 

![New Project](./images/01_01_new_project.gif)

Project를 사용하면 R 스크립트 코드를 모아놓을 수 있고, 프로젝트별로 옵션을 설정하는 등 여러 편리한 점이 있습니다.

![Project Option](./images/01_02_project_option.png)

프로젝트에 새로운 R 스크립트 파일을 생성하려면 아래 메뉴를 선택합니다.

![New File](./images/01_03_new_file.png)

새로운 R 스크립트 파일에 R 코드를 작성합니다. 작성한 코드를 실행하려면 실행하려는 행에 커서를 두고 Ctrl-Enter를 누릅니다. 여러 행을 블럭 지정하여 실행할 수도 있습니다. 파일 전체를 실행하려면 Ctrl-Alt-R 키를 누릅니다.

![Run selection](./images/01_04_run_selection.png)

기본 R에는 사회연결망 분석 기능이 없기 때문에, 사회연결망 분석을 지원하는 패키지를 별도로 설치해야 합니다. R에서 사회연결망 분석을 지원하는 패키지 중 많이 사용되는 것에는 크게 다음과 같은 두 가지가 있습니다.

 * sna
 * igraph

이외에도 시계열 분석이라던지 통계적 네트워크 분석을 지원하는 다양한 패키지들도 존재합니다.

 * [statnet](http://statnet.csde.washington.edu/): Statistical network analysis 
 * RSiena
 * tnet
 * [기타 네트워크 관련 패키지들](http://www.bojanorama.pl/_media/snar:rsna-slides.pdf)


이번 실습에서는 igraph를 사용합니다. igraph는 일반적인 C 라이브러리 형태로 제작되어 R 뿐만 아니라 다양한 소프트웨어에서 사용되고 있고, sna 패키지에 비해 분석 성능이나 다룰 수 있는 데이터의 규모 측면에서 뛰어납니다.

igraph를 설치하기 위해서, Console에서 다음 명령을 수행합니다.

```{r, eval=FALSE}
install.packages("igraph")
```

또는 다음의 GUI를 사용합니다.

![Install Packages](./images/01_05_package_install.gif)

패키지를 설치한 후에는, 현재 워크스페이스에서 해당 패키지를 사용하겠다는 선언을 해줘야 합니다. 선언은 다음과 같이 합니다.

```{r}
library(igraph)
```

----

## 네트워크 생성하기

첫번째 날인 오늘은, 네트워크 분석을 위해 가장 처음 필요한, 네트워크를 생성하거나 불러오고, 분석을 수행하고, 저장하는 방법을 알아봅니다.

우선 네트워크를 만드는 방법을 알아봅니다.

네트워크를 만들 수 있는 방법은 여러가지가 있습니다. 가장 간단한 방법은 분석자가 임의의 데이터를 만들어서 입력해주는 방법입니다. 

```
1 -> 2
1 -> 3
2 -> 3
3 -> 4
```

위와 같은 edgelist를 네트워크 데이터로 만들려면 어떻게 할까요?

우선 짝수개의 배열을 만듭니다.

```{r}
v <- c(1,2, 1,3, 2,3, 3,4)
v
```

위와 같은 1차원의 배열을 graph 함수에 넣으면, 두 원소씩을 한 쌍의 링크로 하는 네트워크가 만들어집니다.

```{r}
g <- graph(v, directed=T)
```

네트워크가 어떻게 생겼는지 맵으로 확인해보려면 ``plot`` 함수를 사용합니다.

```{r}
plot(g)
```

위와 같은 방법을 통해, 네트워크를 만들고 그릴 수 있습니다. 하지만 대부분의 경우에는 네트워크를 직접 수동으로 입력하지 않고 CSV 등의 데이터 파일로부터 가져오게 됩니다. CSV 파일을 불러들이는 방법은 오늘 마지막 부분에서 다루겠습니다.

당분간의 실습은 R에서  패키지 형태로 제공되는 예제 데이터를 사용하겠습니다.

우선, 예제 데이터 패키지를 설치합니다.

```{r, eval=FALSE}
install.packages("igraphdata")
```

그리고 네트워크들 중 하나를 불러들입니다.

```{r}
library("igraphdata")
data(UKfaculty)
```

데이터를 확인해봅니다.

```{r}
summary.igraph(UKfaculty)
plot(UKfaculty)
```

노드가 81개, 링크가 817개인 네트워크입니다. 노드 속성과 링크 속성도 존재합니다.

위의 출력 내용을 해석해보면 다음과 같습니다.

 * IGRAPH라는 문자열은 이 데이터가 igraph 데이터라는 것을 표시합니다.
 * D-W- 에서 첫번째 문자인 D는, 방향성이 있는(directed) 네트워크라는 것을 뜻합니다. 이 자리에는 D 또는 U(undirected)가 올 수 있습니다.
 * 두번째 자리에는 N 또는 -이 옵니다. 노드 속성 중에 name이라는 속성이 있는 경우는 '이름붙여진(named)' 네트워크라는 의미로 N으로 표시해줍니다.
 * 세번째 자리의 W는 링크 세기(weight)가 있는 네트워크라는 것을 뜻합니다.
 * 네번째 자리에는 B 또는 -이 옵니다. Bipartite 네트워크인 경우에 B로 표시합니다.

아랫줄에는 속성 목록이 표시됩니다. 속성 목록은 "속성 이름 (종류/데이터형식)"의 순서로 표시됩니다.

종류에는 g, v, e가 있습니다. g는 graph, v는 vertex, e는 edge에 대한 속성이라는 것을 표시합니다.

데이터형식에는 c, n, l, x가 있습니다. c는 문자 데이터(character), n은 숫자 데이터(numeric), l은 논리 데이터(logical), o는 기타(other)를 의미합니다.

데이터가 어떻게 생겼는지 한 번 살펴보겠습니다.

```{r }
nodes_df <- get.data.frame(UKfaculty, what="vertices")
edges_df <- get.data.frame(UKfaculty, what="edges")
head(nodes_df, 5)
head(edges_df, 5)
```

이제 간단하게 네트워크 분석이 어떻게 이루어지는지 살펴봅시다.

## 네트워크 기본 속성 알아보기

우선, 네트워크의 밀도와 지름, 평균 도달 거리, 컴포넌트 구조(strong component)를 살펴봅시다.

```{r}
graph.density(UKfaculty)
diameter(UKfaculty)
average.path.length(UKfaculty)
clusters(UKfaculty, mode="strong")
```

그리고, 네트워크 각 노드들의 Degree 분포를 살펴봅니다.

```{r}
deg <- degree(UKfaculty)
df <- as.data.frame(table(deg))
df
plot(df)
```

노드별 degree 값을 노드 속성으로 추가하려면 아래와 같이 합니다.

```{r}
V(UKfaculty)$degree <- deg
UKfaculty
```

degree라는 이름의 노드 속성이 추가된 것을 볼 수 있습니다.

## 간략한 R 문법 (1)

지금까지 나온 R 문법 중 한두가지 정도만 잠깐 설명하고 넘어가도록 하겠습니다.

우선, ``<-``라는 표시가 몇번 나왔습니다. 이 표시는 할당을 의미하는 표시입니다.

```{r }
num_boys <- 3
num_girls <- 5
num_people <- num_boys + num_girls
```

또 살펴볼 표시는 ``$``라는 표시입니다. 이 표시는 이름 있는 하위 데이터에 대한 접근자입니다. 위에서 Degree 수치를 구한 결과가 Data Frame으로 표 형태로 저장되어 있습니다. 여기서 첫번째 컬럼을 접근하려면 아래와 같은 두 가지 방법이 있습니다.

```{r }
df[1]
df$deg
```

네트워크에 관련해서 살펴볼 것은 ``V()``와 ``E()`` 입니다. ``V()``는 그래프의 모든 노드들을 반환합니다. ``E()``는 그래프의 모든 링크들을 반환합니다.

``$`` 접근자와 연관해서 생각해봅시다. 

```{r }
UKfaculty$diameter <- diameter(UKfaculty)
UKfaculty
UKfaculty$diameter
```

위의 명령을 해석해보면, UKfaculty 네트워크의 지름을 구해서, diameter라는 이름의 하위 변수를 만들어 저장하라는 뜻입니다. ``UKfacuilty$name`` 형태와 같이 하위 변수를 저장하면, 그 변수는 네트워크 레벨에서 저장합니다.

반면, 아래의 경우를 봅시다.

```{r }
V(UKfaculty)$age <- 30
V(UKfaculty)$age
```

이번에도 하위 변수를 만드는데, 네트워크 레벨 말고, 노드 레벨에 대해서 만듭니다. 각 노드에 age라는 하위 변수를 만들어서 30이라는 값을 부여합니다.

## 네트워크 스타일링하기

네트워크 맵을 스타일링하는 방법을 살펴봅니다.

우선 맵 전체에 적용되는 모양을 바꿔봅시다. 화살표의 크기를 좀 줄이고, 링크의 색을 좀 연하게, 그리고 노드들을 좀 더 넓게 분포시켜봅시다.

```{r}
V(UKfaculty)$size <- 5
V(UKfaculty)$label <- NA
E(UKfaculty)$arrow.size <- 0.3
E(UKfaculty)$color <- "#33333333"
plot(UKfaculty, layout=layout.fruchterman.reingold)
```

노드 속성 'size'는 스타일링을 위해 미리 정의되어 있는 속성입니다. 이 속성에 5라는 값을 할당해주었습니다. 역시 링크 속성 'arrow'는 링크의 화살표의 스타일링을 위한 시스템 속성입니다. 여기에 0.3을 할당해주었습니다. 링크 속성 'color'에는 #33333333으로 회색을 약간 투명하게 넣어주었습니다. 마지막으로 맵을 그릴 때, FR 레이아웃을 사용하도록 그려주었습니다.

이번에는 노드의 속성에 따라 색상을 다르게 그려봅시다. UKfaculty 네트워크 데이터에는 Group이라는 속성이 들어있습니다. 이 Group에 따라서 노드의 색을 다르게 줘봅시다.

```{r}
V(UKfaculty)$color <- V(UKfaculty)$Group + 1
plot(UKfaculty, layout=layout.fruchterman.reingold)
```

노드 속성 'color'에 따라서 색상이 결정됩니다. 색상에는 직접 RGB 값을 넣어줄 수도 있고, 1부터 시작하는 정수를 넣어주면 igraph에 미리 정의된 색상들을 사용합니다.

----

# 네트워크 변형하기

네트워크에서 특정 노드나 링크를 제거하거나 서브 네트워크를 추출하는 등, 네트워크를 다루는 방법에 대해 알아봅니다.

## 조건에 맞는 노드/링크 추출하기

조건에 맞는 노드/링크를 추출하는 방법을 먼저 알아봅니다.

노드, 링크를 다룰 때는, ``V()``와 ``E()`` 함수가 요긴하게 사용됩니다. 아래는 UKfaculty 네트워크의 모든 노드와 모든 링크를 출력합니다. (단, 모든 링크를 전부 출력하면 양이 많아서 10개만 출력하도록 했습니다.)

```{r }
V(UKfaculty)
E(UKfaculty)[1:10]
```

여기서, 노드나 링크의 속성을 기준으로 조건을 만족하는 노드/링크들만을 선택하는 방법을 살펴봅니다. 우선 어떤 속성들이 있는지 알아봐야겠죠.

```{r }
list.vertex.attributes(UKfaculty)
list.edge.attributes(UKfaculty)
```

노드와 링크에는 각각 위와 같은 속성들이 존재하는 것을 알 수 있습니다.

이 중에서, 노드의 Group 속성을 기준으로 노드를 선별해봅시다. 우선 Group에 값들이 어떻게 들어가 있는지 확인해봅니다.

```{r }
V(UKfaculty)$Group
factor(V(UKfaculty)$Group)
```

Group은 숫자로 이루어져 있고, 고유값은 1, 2, 3, 4로 이루어져 있습니다. 이 중에서 Group 번호가 2인 노드들만을 추출해봅니다.

```{r }
V(UKfaculty)[Group == 2]
```

``V()``를 사용하여 네트워크의 노드들에 대해 질의를 하는데, [] 안에 조건을 써주었습니다. 조건의 내용은 Group의 값이 2와 같은 (=가 두개인 것에 주의) 경우로 주었습니다.

결과로 나온 값들은 각 노드의 고유번호(ID)입니다. ID가 5, 6, 7, 10, ... 등인 노드들이 질의 조건에 맞는 노드들입니다.

한 경우를 더 살펴보죠. Degree 값이 30 이상인 노드들이 어떤 노드들인지 살펴보겠습니다.

```{r }
V(UKfaculty)[degree > 30]
```

링크의 경우도 마찬가지입니다. 링크는 ``E()``를 사용합니다.

```{r }
E(UKfaculty)[weight > 15]
```

조건에 맞는 링크들을 확인할 수 있습니다.

## 노드/링크 조건으로 서브 네트워크 추출하기

조건에 맞는 노드들을 선택하는 방법을 살펴보았는데, 이번에는 네트워크에서 그 노드들만을 추출해서 서브 네트워크를 만드는 방법을 알아보겠습니다.

```{r }
group_two_nodes <- V(UKfaculty)[Group == 2]
group_two_network <- induced.subgraph(UKfaculty, group_two_nodes)
group_two_network
plot(group_two_network)
```

Group 속성 값이 2인 노드들로 이루어진 새 네트워크는, 노드가 27개, 링크가 250개인 것을 알 수 있습니다.

링크 조건을 사용해서도 새로운 네트워크를 만들 수 있습니다.

```{r }
high_weight_edges <- E(UKfaculty)[weight > 15]
high_weight_network <- subgraph.edges(UKfaculty, high_weight_edges)
high_weight_network
plot(high_weight_network)
```

## 특정 조건의 노드/링크 제거하기

분석 과정에서, 특정 조건의 노드나 링크를 제거해야 하는 경우가 있습니다. Proximity measure를 통해 네트워크를 구성하는 경우에 cut-off 값을 적용하는 경우가 대표적입니다. 이런 경우에는 ``delete.vertices``와 ``delete.edges`` 함수를 사용합니다.

예제로, weight가 1인 링크들을 제거해보겠습니다.

```{r }
low_weight_edges <- E(UKfaculty)[weight == 1]
high_weight_network <- delete.edges(UKfaculty, low_weight_edges)
plot(high_weight_network)
```

새로 생긴 네트워크는 링크가 615개로 축소된 것을 알 수 있습니다. 

# 데이터 입출력하기

분석 과정에서 R만을 사용하는 경우는 거의 드뭅니다. 데이터를 수집하는데에는 엑셀이나 텍스트 에디터를 사용하고, 복잡하거나 규모가 큰 네트워크(약 노드 1000개 이상 규모)를 시각화할 때는 pajek이나 Gephi 등을 사용합니다. 이렇듯 분석 과정에 소용되는 다른 도구와의 연계를 어떻게 하는지 몇 가지 경우를 살펴보겠습니다.

## CSV 파일로부터 네트워크 불러들이기

다음과 같은 CSV 형태의 edge list 파일이 있다고 합시다.

sample_edge_list.csv

```
from,to,금액,형태
김철수,송아영,3000,현금
김철수,홍길동,23000,카드
김철수,서지우,8500,카드
김철수,김병길,900,현금
송아영,김병길,10000,카드
송아영,서지우,300,현금
서지우,홍길동,100000,수표
홍길동,김병길,5000,현금
```

첫번째 행을 살펴보자면, '김철수가 송아영에게 현금으로 3000원을 지불했다'라고 할 수 있겠습니다.

이 CSV 파일을 네트워크로 만들기 위해서는, 우선 R의 DataFrame 형태로 불러들입니다.

```{r}
my_edgelist <- read.csv('sample_edge_list.csv', fileEncoding='utf-8')
my_edgelist
```

위와 같이, 파일을 불러들일 때는 ``read.csv()`` 함수를 사용합니다. 첫번째 인자로 불러들일 파일의 이름을, 두번째 인자로는 파일의 인코딩을 써줍니다. 영문 파일일 때는 인코딩 인자를 생략할 수 있습니다. CSV 파일의 첫 행은 헤더로 인식합니다. 헤더가 없는 파일의 경우에는 header를 FALSE로 주고, col.names 인자에 컬럼의 헤더를 직접 입력해줍니다.

```{r}
my_edgelist2 <- read.csv('sample_edge_list.csv', header=FALSE, col.names=c('에서', '으로', '금액', '형태'), fileEncoding='utf-8')
my_edgelist2
```

이렇게 R의 DataFrame 형태로 데이터를 불러들였으면, 아래 명령으로 네트워크로 변환합니다.

```{r}
my_graph <- graph.data.frame(my_edgelist, directed=TRUE)
my_graph
plot(my_graph)
```

Edgelist 뿐만 아니라 노드 속성 데이터도 있는 경우를 살펴봅니다.

우선 edgelist 때와 같이, 노드 속성 데이터도 DataFrame으로 불러들입니다. 데이터는 아래와 같습니다.

sample_nodeset.csv
```
이름,나이,자산규모,소속그룹
김철수,30,5000,A
서지우,25,300,A
송아영,33,1000,C
홍길동,40,300,B
김병길,12,6000,C
```

```{r}
my_nodeset <- read.csv('sample_nodeset.csv', fileEncoding='utf-8')
my_nodeset
```

그리고, 네트워크를 만들 때, vertices 인자를 통해 노드셋 정보를 함께 넣어줍니다.

```{r}
my_graph2 <- graph.data.frame(my_edgelist, directed=TRUE, vertices=my_nodeset)
print(my_graph2)
```

속성 정보에 노드 속성이 추가된 것을 볼 수 있습니다.

이상과 같이 CSV 파일로부터 링크 데이터와 노드 데이터를 불러들여 네트워크를 구성할 수 있습니다.

----

이번에는 반대로, R에서 구성된 네트워크 데이터를 CSV 파일로 저장하는 방법을 알아보겠습니다. 원본 CSV 데이터를 가지고 R에서 네트워크 분석을 하는 동안, 추가적으로 링크나 노드들에 대한 부가 정보들이 생성될 수 있습니다. 이것을 파일로 보존해야 하는 경우가 종종 생깁니다.

네트워크를 CSV 파일로 저장하기 위해서는 다음과 같이 합니다.

```{r}
nodes_df <- get.data.frame(UKfaculty, what="vertices")
edges_df <- get.data.frame(UKfaculty, what="edges")
```

위와 같이 우선 네트워크를 R의 DataFrame 데이터 형태로 변환합니다. 그리고 아래와 같이 파일로 저장합니다.

```{r}
write.csv(nodes_df, file='vertices_ukfaculty.csv')
write.csv(edges_df, file='edges_ukfaculty.csv', row.names=FALSE)
```

----

# 시각화 도구와 연계하기

간단한 네트워크 맵은 R에서 직접 시각화가 가능하지만, 네트워크가 큰 경우는 그리기가 어렵습니다. 또한 미려한 스타일을 얻기 위해서는 많은 노력이 필요합니다. 이런 경우에는 네트워크 맵을 그리는 다른 도구를 사용하는 것이 좋습니다. 

## Pajek 파일 형식으로 내보내기

Pajek 파일 형식으로 내보내는 방법은 간단합니다.

```{r eval=FALSE}
write.graph(UKfaculty, "UKfaculty.net", format="pajek")
```

## Gephi 파일 형식으로 내보내기

Gephi로 데이터를 내보내는 방법을 설명합니다. 우선 다음과 같이 패키지를 설치합니다.

```{r eval=FALSE}
install.packages('rgexf')
```

``write.rgexf``라는 함수를 사용할텐데, 이 함수에는 4가지 인자를 넘겨줘야 합니다.

 * 노드 목록 (node ID, node label 형태의 2개 컬럼을 가지는 DataFrame)
 * 링크 목록 (source node ID, target node ID 형태의 2개 컬럼을 가지는 DataFrame)
 * 노드 속성들
 * 링크 속성들

우선 네트워크를 DataFrame 형태로 변환합니다.

```{r }
nodes_df <- get.data.frame(UKfaculty, what="vertices")
edges_df <- get.data.frame(UKfaculty, what="edges")
```

아래 명령으로 노드 목록과 링크 목록을 얻습니다.

```{r eval=FALSE}
nodes_df$id <- row.names(nodes_df)
nodes_df[,c('id', 'id')]
edges_df[1:2]
```

UKfaculty 네트워크를 DataFrame으로 변환하면, 노드 ID에 해당하는 컬럼이 별도로 없고, row 헤더로 들어가 있습니다. 이것을 컬럼의 하나로 삽입해주었습니다. 그리고 레이블이 따로 없기 때문에 id를 이름 컬럼으로 입력해줍니다.

아래 명령으로 노드 속성과 링크 속성을 얻습니다.

```{r eval=FALSE}
nodes_df[1:5]
edges_df[3]
```

최종적으로, ``write.gexf`` 명령과, 위에서 알아낸 네 요소를 구하는 방법을 사용하여 gexf 파일을 생성합니다.

```{r eval=FALSE}
library('rgexf')
write.gexf(output='ukfaculty.gexf', nodes=nodes_df[,c('id', 'id')], edges=edges_df[1:2], nodesAtt=nodes_df[1:5], edgesAtt=edges_df[3])
```

----

이상으로 R에서 네트워크 데이터를 입출력하는 방법, 그리고 네트워크를 다루는 방법에 대해서 간단히 살펴보았습니다.

다음 시간에는 본격적으로 사회 연결망 분석 지표들을 사용하는 방법을 알아보겠습니다.
