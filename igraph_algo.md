알고리즘

Network Property

 * is.connected
 * bipartite.mapping
 * clique.number
 * diameter
 * average.path.length
 * eccentricity
 * graph.coreness
 * graph.density
 * has.multiple
 * modularity
 * transitivity: clustering coeffcient
 * reciprocity

Centrality

 * degree
 * betweenness
 * closeness
 * evcent: eigenvector centrality
 * alpha.centrality
 * bonpow: bonacci power
 * edge.betweenness
 * hub.score: hub/authority
 * constraint: structural hole

Centralization

 * centralization.betweenness
 * centralization.closeness
 * centralization.degree
 * centralization.evcent

Commmunity

 * edge.betweenness.community
 * fastgreedy.community
 * label.propagation.community
 * leading.eigenvector.community
 * multilevel.community
 * optimal.community
 * spinglass.community
 * walktrap.community

Community - post process

 * as.dendrogram.communities
 * as.hclust.communities
 * asPhylo.communities

Cohesion

 * cliques
 * maximal.cliques
 * clusters: components
 * cohesiveBlocks
 * blockGraphs
 * maxcohesion

Dyad / Triad

 * dyad.census
 * triad.census

Misc

 * power.law.fit

Graph Manipulation

 * subgraph
 * decompose.graph
 * delete.edges
 * delete.vertices
 * add.edges
 * add.vertices
 * get.adjlist
 * get.data.frame
 * get.edge
 * get.graph.attribute
 * simplify
 * symmetrize
 * ...
